package com.artcool.coolive_sdk.camera;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.DrawableRes;
import android.util.TypedValue;

import com.artcool.coolive_sdk.app.Coolive;
import com.orhanobut.logger.Logger;
import com.tencent.rtmp.TXLiveConstants;
import com.tencent.rtmp.TXLivePushConfig;
import com.tencent.rtmp.TXLivePusher;
import com.tencent.rtmp.ui.TXCloudVideoView;


/**
 * 摄像头推流
 *
 * @author wuyibin
 * @date 2019/7/24
 */
public class CameraPush {

    private static CameraPush cameraPush = null;
    private static TXLivePushConfig mLivePushConfig;
    private static TXLivePusher mPusher;

    private CameraPush(){}

    public static CameraPush getInstance() {
        if (cameraPush == null) {
            synchronized (CameraPush.class) {
                if (cameraPush == null) {
                    cameraPush = new CameraPush();
                }
                return cameraPush;
            }
        } else {
            return cameraPush;
        }
    }

    /**
     * 初始化推流组件
     */
    public void init() {
        mLivePushConfig = new TXLivePushConfig();
        mPusher = new TXLivePusher(Coolive.getApplicationContext());
        mPusher.setConfig(mLivePushConfig);
    }

    public void startPreview(TXCloudVideoView view) {
        mPusher.startCameraPreview(view);
    }

    /**
     * 启动推流
     */
    public void startPush(String rtmpUrl) {
        int result = mPusher.startPusher(rtmpUrl.trim());
        if (result == -5) {
            Logger.i("Coolive : startRTMPPush: license 校验失败");
        }
    }

    /**
     * 结束推流
     */
    public void stopPush() {
        mPusher.stopPusher();
        //关闭摄像头预览
        mPusher.stopCameraPreview(true);
    }

    /**
     * 纯音频推流
     *
     * @param rtmpUrl
     */
    public void pureAudioPush(String rtmpUrl) {
        mLivePushConfig.enablePureAudioPush(true);
        mPusher.setConfig(mLivePushConfig);
        mPusher.startPusher(rtmpUrl.trim());
    }

    /**
     * 设置画面清晰度
     * 1、秀场直播：VIDEO_QUALITY_HIGH_DEFINITION 或 VIDEO_QUALITY_SUPER_DEFINITION ，false
     * 2、手游直播：VIDEO_QUALITY_SUPER_DEFINITION，true
     * 3、连麦（主画面）：VIDEO_QUALITY_LINKMIC_MAIN_PUBLISHER，true
     * 4、连麦（小画面）：VIDEO_QUALITY_LINKMIC_SUB_PUBLISHER，false
     * 5、手动设置
     * 6、根据网络状态切换自动清晰度:
     * 弱网：采取标清
     * 正常：1、移动网络：高清
     * 2、wifi：超清
     * 可以根据策略自行实现
     *
     * @param type
     * @param adjustBitrate
     */
    public void setVideoQuality(int type, boolean adjustBitrate) {
        mPusher.setVideoQuality(type, adjustBitrate, false);
    }

    /**
     * 美颜美白和红润特效
     *
     * @param style          美颜算法：  0：光滑  1：自然  2：朦胧
     * @param beautyLevel    磨皮等级： 取值为 0-9.取值为 0 时代表关闭美颜效果.默认值: 0,即关闭美颜效果.
     * @param whiteningLevel 美白等级： 取值为 0-9.取值为 0 时代表关闭美白效果.默认值: 0,即关闭美白效果.
     * @param ruddyLevel     红润等级： 取值为 0-9.取值为 0 时代表关闭美白效果.默认值: 0,即关闭美白效果.
     */
    public void setVideoBeautyFilter(int style, int beautyLevel, int whiteningLevel, int ruddyLevel) {
        mPusher.setBeautyFilter(style, beautyLevel, whiteningLevel, ruddyLevel);
    }


    /**
     * 色彩滤镜效果
     *
     * @param resources
     * @param id
     */
    public void setColorFilter(Resources resources, @DrawableRes int id) {
        Bitmap bitmap = decodeResource(resources, id);
        mPusher.setFilter(bitmap);
        mPusher.setSpecialRatio(0.5f);
    }

    @SuppressLint("ResourceType")
    private Bitmap decodeResource(Resources resources, @DrawableRes int id) {
        TypedValue value = new TypedValue();
        resources.openRawResource(id, value);
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inTargetDensity = value.density;
        return BitmapFactory.decodeResource(resources, id, opts);
    }

    /**
     * 切换前后摄像头
     */
    public void switchCamera() {
        mPusher.switchCamera();
    }

    /**
     * 打开或关闭闪光灯
     *
     * @param isOpen
     */
    public void turnOnFlashLight(boolean isOpen) {
        mPusher.turnOnFlashLight(isOpen);
    }

    /**
     * 调整摄像头的焦距
     *
     * @param value
     */
    public void setZoom(int value) {
        mPusher.setZoom(value);
    }

    /**
     * 设置曝光比例，取值范围从-1到1
     * <p>
     * 负数表示调低曝光，-1是最小值，对应getMinExposureCompensation。
     * 正数表示调高曝光，1是最大值，对getMaxExposureCompensation。
     * 0表示不调整曝光，默认值为0。
     *
     * @param value
     */
    public void setExposureCompensation(float value) {
        mPusher.setExposureCompensation(value);
    }

    /**
     * 横屏推流
     *
     * @param isPortrait
     */
    public void setOrientationChange(boolean isPortrait) {
        if (isPortrait) {
            mLivePushConfig.setHomeOrientation(TXLiveConstants.VIDEO_ANGLE_HOME_DOWN);
            mPusher.setConfig(mLivePushConfig);
            mPusher.setRenderRotation(0);
        } else {
            mLivePushConfig.setHomeOrientation(TXLiveConstants.VIDEO_ANGLE_HOME_RIGHT);
            mPusher.setConfig(mLivePushConfig);
            // 因为采集旋转了，为了保证本地渲染是正的，则设置渲染角度为90度。
            mPusher.setRenderRotation(90);
        }
    }

    /**
     * 开启隐私模式
     *
     * @param resources 资源
     * @param id        资源文件
     */
    public void openPriveteMode(Resources resources, @DrawableRes int id) {
        Bitmap bitmap = decodeResource(resources, id);
        mLivePushConfig.setPauseImg(bitmap);
        mLivePushConfig.setPauseImg(300, 5);

        //表示仅暂停视频采集，不暂停音频采集
        //mLivePushConfig.setPauseFlag(PAUSE_FLAG_PAUSE_VIDEO);
        //表示同时暂停视频和音频采集
        mLivePushConfig.setPauseFlag(TXLiveConstants.PAUSE_FLAG_PAUSE_VIDEO | TXLiveConstants.PAUSE_FLAG_PAUSE_AUDIO);
        mPusher.setConfig(mLivePushConfig);
    }

    /**
     * 进入隐私模式
     */
    public void pausePusher() {
        mPusher.pausePusher();
    }

    /**
     * 退出隐私模式
     */
    public void exitPrivateMode() {
        mPusher.resumePusher();
    }

    /**
     * 设置logo水印
     *
     * @param resources
     * @param id
     */
    public void setLogo(Resources resources, @DrawableRes int id) {
        Bitmap bitmap = decodeResource(resources, id);
        mLivePushConfig.setWatermark(bitmap, 0.02f, 0.05f, 0.2f);
        mPusher.setConfig(mLivePushConfig);
    }
}
