package com.artcool.coolive_sdk.screen;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.artcool.coolive_sdk.app.Coolive;
import com.orhanobut.logger.Logger;
import com.tencent.rtmp.TXLivePushConfig;
import com.tencent.rtmp.TXLivePusher;

/**
 *  录屏推流
 * @author wuyibin
 * @date 2019/7/24
 */
public class ScreenPush {

    private static ScreenPush screenPush = null;
    private TXLivePusher mPusher;
    private TXLivePushConfig mPushConfig;

    private ScreenPush(){}

    public static ScreenPush getInstance() {
        if (screenPush == null) {
            synchronized (ScreenPush.class) {
                if (screenPush == null) {
                    screenPush = new ScreenPush();
                }
                return screenPush;
            }
        } else {
            return screenPush;
        }
    }

    /**
     * 初始化 Pusher 对象
     */
    public void init() {
        mPusher = new TXLivePusher(Coolive.getApplicationContext());
        mPushConfig = new TXLivePushConfig();
        mPusher.setConfig(mPushConfig);
    }

    /**
     * 启动推流
     * @param rtmpUrl
     */
    public void startPush(String rtmpUrl) {
        int result = mPusher.startPusher(rtmpUrl);
        Logger.i("startPush: " + result);
        mPusher.startScreenCapture();
    }

    /**
     * 结束推流
     */
    public void stopPush() {
        mPusher.stopPusher();
        mPusher.stopScreenCapture();
    }

    /**
     * 设置一张等待图片
     * @param bitmap
     */
    public void setPauseImage(Bitmap bitmap) {
       mPushConfig.setPauseImg(bitmap);
    }

    /**
     * 设置视频水印
     * @param resources
     * @param id
     */
    public void setLogo(Resources resources,int id) {
        mPushConfig.setWatermark(BitmapFactory.decodeResource(resources, id), 10, 10);
        mPusher.setConfig(mPushConfig);
    }

}
