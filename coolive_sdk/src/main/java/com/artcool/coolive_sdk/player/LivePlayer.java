package com.artcool.coolive_sdk.player;

import com.artcool.coolive_sdk.app.Coolive;
import com.tencent.rtmp.TXLiveConstants;
import com.tencent.rtmp.TXLivePlayer;
import com.tencent.rtmp.ui.TXCloudVideoView;

/**
 * 直播拉流
 * @author wuyibin
 * @date 2019/7/24
 */
public class LivePlayer {

    private static LivePlayer mLivePlayer = null;
    private TXLivePlayer mPlayer;

    private LivePlayer(){}

    public static LivePlayer getInstance() {
        if (mLivePlayer == null) {
            synchronized (LivePlayer.class) {
                if (mLivePlayer == null) {
                    mLivePlayer = new LivePlayer();
                }
                return mLivePlayer;
            }
        } else {
            return mLivePlayer;
        }
    }

    /**
     * 初始化创建player
     * @param view
     */
    public TXLivePlayer init(TXCloudVideoView view) {
        mPlayer = new TXLivePlayer(Coolive.getApplicationContext());
        mPlayer.setPlayerView(view);
        return mPlayer;
    }

    /**
     * 启动播放
     * 0：rtmp
     * 1：flv
     * 5：rtmp_acc
     * 3:hls
     * @param url
     * @param type
     */
    public void startPlay(String url,int type) {
       mPlayer.startPlay(url, type);
    }

    /**
     * 将图像等比例铺满整个屏幕，多余部分裁剪掉，此模式下画面不会留黑边，但可能因为部分区域被裁剪而显示不全
     */
    public void setFullScreen() {
        mPlayer.setRenderMode(TXLiveConstants.RENDER_MODE_FULL_FILL_SCREEN);
    }

    /**
     * 将图像等比例缩放，适配最长边，缩放后的宽和高都不会超过显示区域，居中显示，画面可能会留有黑边
     */
    public void setAdjustScreen() {
        mPlayer.setRenderMode(TXLiveConstants.RENDER_MODE_ADJUST_RESOLUTION);
    }

    /**
     * 设置画面渲染方向
     * @param rotation  方向
     */
    public void setRotation(int rotation) {
        mPlayer.setRenderRotation(rotation);
    }

    /**
     * 暂停播放
     */
    public void pausePlay() {
        mPlayer.pause();
    }

    /**
     * 暂停之后，继续播放
     */
    public void resumePlay() {
        mPlayer.resume();
    }

    /**
     * 结束播放
     * @param view
     */
    public void stopPlay(TXCloudVideoView view) {
        mPlayer.stopPlay(true);
        view.onDestroy();
    }

}
