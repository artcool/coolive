package com.artcool.coolive_sdk.app;

import android.app.Activity;
import android.os.Handler;

import com.artcool.coolive_sdk.camera.CameraPush;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.tencent.rtmp.TXLiveBase;

import java.util.HashMap;



/**
 * 配置管理类
 * @author wuyibin
 * @date 2019/7/24
 */
public class Configurator {

    private static final HashMap<Object,Object> ARTCOOL_CONFIGS = new HashMap<>();
    private static final Handler HANDLER = new Handler();

    private Configurator() {
        ARTCOOL_CONFIGS.put(ConfigKeys.CONFIG_READY,false);
        ARTCOOL_CONFIGS.put(ConfigKeys.HANDLER,HANDLER);
    }

    static Configurator getInstance() {
        return Holder.INSTANCE;
    }

    private static class Holder {
        private static final Configurator INSTANCE = new Configurator();
    }

    public final void configure() {
        Logger.addLogAdapter(new AndroidLogAdapter());
        ARTCOOL_CONFIGS.put(ConfigKeys.CONFIG_READY,true);
        Logger.i(Coolive.getLicenseKey() + ";" +  Coolive.getLicenseUrl());
        TXLiveBase.getInstance().setLicence(Coolive.getApplicationContext(),Coolive.getLicenseUrl(),Coolive.getLicenseKey());
    }


    final HashMap<Object,Object> getArtcoolConfigs() {
        return ARTCOOL_CONFIGS;
    }

    /**
     * 配置腾讯直播的license_url
     * @param licenseUrl
     * @return
     */
    public final Configurator withLicenseUrl(String licenseUrl) {
        ARTCOOL_CONFIGS.put(ConfigKeys.LICENSE_URL,licenseUrl);
        return this;
    }

    /**
     * 配置腾讯直播的license_key
     * @param licenseKey
     * @return
     */
    public final Configurator withLicenseKey(String licenseKey) {
        ARTCOOL_CONFIGS.put(ConfigKeys.LICENSE_KEY,licenseKey);
        return this;
    }

    private void checkConfiguration() {
        final boolean isReady = (boolean) ARTCOOL_CONFIGS.get(ConfigKeys.CONFIG_READY);
        if (!isReady) {
            throw new RuntimeException("Configuration is not ready,call configure");
        }
    }


    final <T> T getConfiguration(Object key) {
        checkConfiguration();
        final Object value = ARTCOOL_CONFIGS.get(key);
        if (value == null) {
            throw new NullPointerException(key.toString() + " IS NULL");
        }
        return (T) ARTCOOL_CONFIGS.get(key);
    }
}
