package com.artcool.coolive_sdk.app;

/**
 *  整个应用程序位置的单例，只能初始化一次
 * @author wuyibin
 * @date 2019/7/24
 */
public enum ConfigKeys {
    API_HOST,
    APPLICATION_CONTEXT,
    CONFIG_READY,
    LICENSE_URL,
    LICENSE_KEY,
    HANDLER
}
