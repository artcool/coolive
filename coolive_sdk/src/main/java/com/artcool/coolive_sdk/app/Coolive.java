package com.artcool.coolive_sdk.app;

import android.content.Context;
import android.os.Handler;


/**
 * 封装一层直播的核心类
 *
 * @author wuyibin
 * @date 2019/7/24
 */
public class Coolive {

    public static Configurator init(Context context) {
        Configurator.getInstance().getArtcoolConfigs().put(ConfigKeys.APPLICATION_CONTEXT,context.getApplicationContext());
        return Configurator.getInstance();
    }

    public static Configurator getConfigurator() {
        return Configurator.getInstance();
    }

    public static <T> T getConfiguration(Object key) {
        return getConfigurator().getConfiguration(key);
    }

    public static Context getApplicationContext() {
        return getConfiguration(ConfigKeys.APPLICATION_CONTEXT);
    }

    public static Handler getHandler() {
        return getConfiguration(ConfigKeys.HANDLER);
    }

    public static String getLicenseUrl() {
        return getConfiguration(ConfigKeys.LICENSE_URL);
    }

    public static String getLicenseKey() {
        return getConfiguration(ConfigKeys.LICENSE_KEY);
    }


}
