package com.artcool.coolive.constant;

/**
 * 常量类
 *
 * @author wuyibin
 * @date 2019/7/24
 */
public class Constants {

    /**
     * 腾讯直播授权key
     */
    public static final String LICENSE_KEY = "d48e808ddfdc7d97fdc1615561d0ed4d";

    /**
     * 腾讯直播授权url
     */
    public static final String LICENSE_URL = "http://license.vod2.myqcloud.com/license/v1/4cd32a3f48dc0e182f9d7fc865d5a2dc/TXLiveSDK.licence";

    /**
     * rtmp推流地址
     */
    public static final String RTMP_PUSH_URL = "rtmp://56822.livepush.myqcloud.com/live/coolive?txSecret=cb6c852c72c707901fe7a1aad1a15656&txTime=5D3F17FF";

    /**
     * 播放地址
     */
    public static final String PLAY_URL = "rtmp://liveplay.86tc.com/live/coolive";



}
