package com.artcool.coolive;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.artcool.coolive.activity.CameraActivity;
import com.artcool.coolive.activity.CameraAndScreenPushActivity;
import com.artcool.coolive.activity.PlayActivity;
import com.artcool.coolive.activity.ScreenPushActivity;
import com.artcool.coolive.base.BaseActivity;
import com.artcool.coolive_sdk.app.Coolive;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wuyb
 */
public class MainActivity extends BaseActivity implements View.OnClickListener {

    String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.SYSTEM_ALERT_WINDOW};
    List<String> mPermissionList = new ArrayList<>();

    @Override
    protected void init() {
        checkPermissions();
    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        setContentView(R.layout.activity_main);
        findViewById(R.id.camera_live).setOnClickListener(this);
        findViewById(R.id.screen_live).setOnClickListener(this);
        findViewById(R.id.both_live).setOnClickListener(this);
        findViewById(R.id.enter_play).setOnClickListener(this);
        findViewById(R.id.both_live_without_floating).setOnClickListener(this);

    }

    private void checkPermissions() {
        for (int i = 0; i < permissions.length; i++) {
            if (ContextCompat.checkSelfPermission(Coolive.getApplicationContext(), permissions[i]) != PackageManager.PERMISSION_GRANTED) {
                mPermissionList.add(permissions[i]);
            }
        }

        if (!mPermissionList.isEmpty()) {
            String[] permissions = mPermissionList.toArray(new String[mPermissionList.size()]);//将List转为数组
            ActivityCompat.requestPermissions(this, permissions, 1);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.camera_live:
                startActivity(new Intent(Coolive.getApplicationContext(), CameraActivity.class));
                break;
            case R.id.screen_live:
                startActivity(new Intent(Coolive.getApplicationContext(), ScreenPushActivity.class));
                break;
            case R.id.both_live:
                Intent floating = new Intent(Coolive.getApplicationContext(), CameraAndScreenPushActivity.class);
                floating.addFlags(0);
                startActivity(floating);
                break;
            case R.id.both_live_without_floating:
                Intent intent = new Intent(Coolive.getApplicationContext(), CameraAndScreenPushActivity.class);
                intent.addFlags(1);
                startActivity(intent);
                break;
            case R.id.enter_play:
                startActivity(new Intent(Coolive.getApplicationContext(), PlayActivity.class));
                break;
            default:
                break;
        }
    }
}
