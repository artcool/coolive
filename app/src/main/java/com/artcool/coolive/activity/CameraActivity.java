package com.artcool.coolive.activity;

import android.view.View;

import com.artcool.coolive.R;
import com.artcool.coolive.base.BaseActivity;
import com.artcool.coolive.constant.Constants;
import com.artcool.coolive_sdk.camera.CameraPush;
import com.tencent.rtmp.ui.TXCloudVideoView;

/**
 *  摄像头推流页面
 * @author wuyibin
 * @date 2019/7/25
 */
public class CameraActivity extends BaseActivity {

    private TXCloudVideoView mCameraPushView;

    @Override
    protected void init() {
        CameraPush.getInstance().init();
    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initData() {
        CameraPush.getInstance().startPush(Constants.RTMP_PUSH_URL);
    }

    @Override
    protected void initView() {
        setContentView(R.layout.activity_camera);
        mCameraPushView = findViewById(R.id.camera_push);
        CameraPush.getInstance().startPreview(mCameraPushView);
    }

    public void close(View view) {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CameraPush.getInstance().stopPush();
    }
}
