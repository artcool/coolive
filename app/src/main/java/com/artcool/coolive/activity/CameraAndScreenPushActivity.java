package com.artcool.coolive.activity;

import android.view.View;

import com.artcool.coolive.R;
import com.artcool.coolive.base.BaseActivity;
import com.artcool.coolive.constant.Constants;
import com.artcool.coolive_sdk.app.Coolive;
import com.artcool.coolive_sdk.camera.CameraPush;
import com.artcool.coolive_sdk.screen.ScreenPush;
import com.orhanobut.logger.Logger;
import com.tencent.rtmp.ui.TXCloudVideoView;
import com.yhao.floatwindow.FloatWindow;
import com.yhao.floatwindow.PermissionListener;
import com.yhao.floatwindow.Screen;
import com.yhao.floatwindow.ViewStateListener;

/**
 *
 * @author wuyibin
 * @date 2019/7/25
 */
public class CameraAndScreenPushActivity extends BaseActivity {

    /**
     * 总结：
     *     1、引入第三方悬浮窗之后，触发播放器监听事件会抢占视频焦点，需要修改对悬浮窗进行优化
     */

    private TXCloudVideoView mCameraView;
    private PermissionListener mPermissionListener = new PermissionListener() {
        @Override
        public void onSuccess() {
            Logger.i("onSuccess");
        }

        @Override
        public void onFail() {
            Logger.i("onFail");
        }
    };
    private ViewStateListener mViewStateListener = new ViewStateListener() {
        @Override
        public void onPositionUpdate(int x, int y) {
            Logger.i("onPositionUpdate" + x + ", y= " + y);
        }

        @Override
        public void onShow() {
            Logger.i("onShow");
        }

        @Override
        public void onHide() {
            Logger.i("onHide");
        }

        @Override
        public void onDismiss() {
            Logger.i("onDismiss");
        }

        @Override
        public void onMoveAnimStart() {
            Logger.i("onMoveAnimStart");
        }

        @Override
        public void onMoveAnimEnd() {
            Logger.i("onMoveAnimEnd");
        }

        @Override
        public void onBackToDesktop() {
            Logger.i("onBackToDesktop");
        }
    };
    private int flags;
    private TXCloudVideoView mCameraVideoView;

    @Override
    protected void init() {
        flags = getIntent().getFlags();
        CameraPush.getInstance().init();
        ScreenPush.getInstance().init();
    }

    @Override
    protected void initEvent() {

    }

    public void close(View view) {
        finish();
    }

    @Override
    protected void initData() {
        CameraPush.getInstance().startPush(Constants.RTMP_PUSH_URL);
        ScreenPush.getInstance().startPush(Constants.RTMP_PUSH_URL);
    }

    @Override
    protected void initView() {
        setContentView(R.layout.activity_camera_screen);
        mCameraVideoView = findViewById(R.id.camera_view);
        if (flags == 0) {
            mCameraVideoView.setVisibility(View.GONE);
            mCameraView = new TXCloudVideoView(Coolive.getApplicationContext());
            CameraPush.getInstance().startPreview(mCameraView);
            FloatWindow
                    .with(Coolive.getApplicationContext())
                    .setView(mCameraView)
                    //设置控件宽高
                    .setWidth(400)
                    .setHeight(400)
                    .setX(Screen.width, 0.6f)
                    .setY(Screen.height, 0.1f)
                    //桌面显示
                    .setDesktopShow(true)
                    //监听悬浮控件状态改变
                    .setViewStateListener(mViewStateListener)
                    //监听权限申请结果
                    .setPermissionListener(mPermissionListener)
                    .build();
        } else {
            mCameraVideoView.setVisibility(View.VISIBLE);
            CameraPush.getInstance().startPreview(mCameraVideoView);
        }



    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CameraPush.getInstance().stopPush();
        ScreenPush.getInstance().stopPush();
        if (flags == 0) {
            FloatWindow.destroy();
        }
    }
}
