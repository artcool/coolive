package com.artcool.coolive.activity;

import android.view.View;

import com.artcool.coolive.R;
import com.artcool.coolive.base.BaseActivity;
import com.artcool.coolive.constant.Constants;
import com.artcool.coolive_sdk.camera.CameraPush;
import com.artcool.coolive_sdk.screen.ScreenPush;

/**
 *
 * @author wuyibin
 * @date 2019/7/25
 */
public class ScreenPushActivity extends BaseActivity {
    @Override
    protected void init() {

    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initData() {
        ScreenPush.getInstance().startPush(Constants.RTMP_PUSH_URL);
    }

    @Override
    protected void initView() {
        setContentView(R.layout.activity_screen);
        ScreenPush.getInstance().init();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ScreenPush.getInstance().stopPush();
    }

    public void close(View view) {
        finish();
    }
}
