package com.artcool.coolive.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.artcool.coolive.R;
import com.artcool.coolive.base.BaseActivity;
import com.artcool.coolive.constant.Constants;
import com.artcool.coolive_sdk.app.Coolive;
import com.artcool.coolive_sdk.player.LivePlayer;
import com.orhanobut.logger.Logger;
import com.tencent.rtmp.ITXLivePlayListener;
import com.tencent.rtmp.TXLiveConstants;
import com.tencent.rtmp.TXLivePlayer;
import com.tencent.rtmp.ui.TXCloudVideoView;


/**
 * 直播播放
 * @author wuyibin
 * @date 2019/7/25
 */
public class PlayActivity extends BaseActivity {

    private TXCloudVideoView mVideoView;

    private TXLivePlayer mPlay;

    @Override
    protected void init() {

    }

    @Override
    protected void initEvent() {
        mPlay.setPlayListener(new ITXLivePlayListener() {
            @Override
            public void onPlayEvent(int i, Bundle bundle) {
                switch (i) {
                    case TXLiveConstants.PLAY_EVT_CONNECT_SUCC:
                        Logger.i("已经连接服务器");
                        break;
                    case TXLiveConstants.PLAY_EVT_PLAY_END:
                        Logger.i("视频播放结束");
                        break;
                    case TXLiveConstants.PLAY_WARNING_READ_WRITE_FAIL:
                        Toast.makeText(Coolive.getApplicationContext(),"当前直播结束，或者未开启",Toast.LENGTH_SHORT).show();
                        finish();
                        break;
                    default:
                        break;
                }

                Logger.i("监听事件" + i);
            }

            @Override
            public void onNetStatus(Bundle bundle) {

            }
        });
    }

    @Override
    protected void initData() {
        LivePlayer.getInstance().startPlay(Constants.PLAY_URL, 0);
    }

    @Override
    protected void initView() {
        setContentView(R.layout.activity_play);
        mVideoView = findViewById(R.id.video_view);
        mPlay = LivePlayer.getInstance().init(mVideoView);
        LivePlayer.getInstance().setFullScreen();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LivePlayer.getInstance().stopPlay(mVideoView);
    }

    public void close(View view) {
        finish();
    }
}
