package com.artcool.coolive.application;

import android.app.Application;

import com.artcool.coolive.constant.Constants;
import com.artcool.coolive_sdk.app.Coolive;
import com.tencent.rtmp.TXLiveBase;
import com.tencent.rtmp.TXLiveConstants;

/**
 *  应用入口
 * @author wuyibin
 * @date 2019/7/24
 */
public class CoolApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Coolive.init(this)
                .withLicenseUrl(Constants.LICENSE_URL)
                .withLicenseKey(Constants.LICENSE_KEY)
                .configure();

    }
}
