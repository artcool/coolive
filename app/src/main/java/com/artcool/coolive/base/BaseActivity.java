package com.artcool.coolive.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

/**
 *  activity基类
 * @author wuyibin
 * @date 2019/7/25
 */
public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
        initView();
        initData();
        initEvent();
    }

    protected abstract void init();

    protected abstract void initEvent();

    protected abstract void initData();

    protected abstract void initView();

}
